# vim: set filetype=rst :

Vocab Drill TODO
----------------

* Better UX design for filter activity

  The whole stuff should be put under a menu option instead of showing it
  every time

* Entry editor

  Should be done in the entry browser

FUTURE PLANS
------------

* Custom entry filter to create customized tests
