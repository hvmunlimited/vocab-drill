package com.ekezet.vocabdrill.dialogs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.exception.DropboxException;
import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.helpers.DbxTools;

public class DbxOpenDialog extends OpenDialog
{
	public class EntryDownloader extends AsyncTask<String, Void, Entry>
	{
		private ProgressDialog mDialog = null;
		private Activity mParent = null;

		public EntryDownloader(Activity parent)
		{
			super();
			mParent = parent;
			mDialog = new ProgressDialog(mParent);
			mDialog.setTitle(R.string.loading_label);
			mDialog.setMessage(getResources().getString(R.string.message_loading_dropbox));
			mDialog.setIndeterminate(true);
		}

		@Override
		protected void onPreExecute()
		{
			mDialog.show();
		}

		@Override
		protected void onPostExecute(Entry result)
		{
			if (mDialog.isShowing())
				mDialog.dismiss();
			if (result == null)
			{
				Toast.makeText(mParent, R.string.error_loading_entries, Toast.LENGTH_SHORT).show();
				return;
			}
			mCurrentEntry = new DbxDialogEntry(result);
			updateList(mCurrentEntry);
		}

		@Override
		protected Entry doInBackground(String... paths)
		{
			Entry entry = DbxTools.getEntry(paths[0]);
			return entry;
		}
	}

	public class DbxDialogEntry extends DialogEntry
	{
		private DropboxAPI.Entry mDbxEntry = null;
		private List<DropboxAPI.Entry> mChildren = null;

		public DbxDialogEntry(DropboxAPI.Entry entry)
		{
			super(entry.path);
			mDbxEntry = entry;
			mChildren = mDbxEntry.contents;
		}

		@Override
		public String getName()
		{
			return mDbxEntry.fileName();
		}

		@Override
		public String getParent()
		{
			return mDbxEntry.parentPath();
		}

		@Override
		public DialogEntry[] getFileList()
		{
			if (mChildren == null)
				return null;
			List<DialogEntry> list = new ArrayList<DialogEntry>();
			for (Entry entry : mChildren)
				if (!entry.isDir && !entry.isDeleted)
					// XXX: do not retrieve children here!
					list.add(new DbxDialogEntry(entry));
			DialogEntry[] result = new DbxDialogEntry[list.size()];
			list.toArray(result);
			return result;
		}

		@Override
		public DialogEntry[] getFolderList()
		{
			if (mChildren == null)
				return null;
			List<DialogEntry> list = new ArrayList<DialogEntry>();
			for (Entry entry : mChildren)
				if (entry.isDir && !entry.isDeleted)
					// XXX: do not retrieve children here!
					list.add(new DbxDialogEntry(entry));
			DialogEntry[] result = new DbxDialogEntry[list.size()];
			list.toArray(result);
			return result;
		}

		@Override
		public boolean isDirectory()
		{
			return mDbxEntry.isDir;
		}

		@Override
		protected boolean isSelected()
		{
			return !isDirectory();
		}

		@Override
		protected boolean exists()
		{
			return !mDbxEntry.isDeleted;
		}
	}

	public class FileDownloader extends AsyncTask<String, Integer, File>
	{
		private ProgressDialog mDialog;
		private String mDropboxPath = null;
		private Activity mParent;

		public FileDownloader(Activity parent)
		{
			mParent = parent;
			mDialog = new ProgressDialog(mParent);
			mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			mDialog.setTitle(R.string.loading_label);
			mDialog.setMessage(getResources().getString(R.string.message_syncing_dropbox));
			mDialog.setProgress(0);
			mDialog.setMax(100);
		}

		@Override
		protected File doInBackground(String... urls)
		{
			if (urls == null || 0 == urls.length)
				return null;
			// generate output path
			final File extCacheDir = Config.getDbxCacheDir();
			if (extCacheDir == null || !extCacheDir.exists())
			{
				Toast.makeText(getApplicationContext(), "Cannot access external storage.", Toast.LENGTH_LONG).show();
				return null;
			}
			final String url = urls[0];
			final File output = new File(extCacheDir, url);
			mDropboxPath = url.replace("/"+output.getName(), "");
			if (mDropboxPath == "")
				mDropboxPath = "/";
			final File parent = output.getParentFile();
			// create parent directories
			parent.mkdirs();
			// save file to disk
			try
			{
				if (!output.exists())
					output.createNewFile();
				DbxTools.getFile(url, output, new ProgressListener()
				{
					@Override
					public void onProgress(long bytes, long total)
					{
						int x = (int) Math.ceil(((float) bytes / (float) total) * 100f);
						publishProgress(x);
					}
				});
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return null;
			}
			catch (DropboxException e)
			{
				e.printStackTrace();
				Log.d("MainActivity", "Trying to get local copy of: " + url);
				if (output.exists())
					return output;
				else
				{
					Toast.makeText(mParent, R.string.error_download, Toast.LENGTH_SHORT).show();
					return null;
				}
			}
			return output;
		}

		@Override
		protected void onPreExecute()
		{
			mDialog.show();
		}

		@Override
		protected void onPostExecute(File result)
		{
			if (mDialog.isShowing())
				mDialog.dismiss();
			if (result == null)
				// TODO: error handling maybe?
				return;
			// pass result back to caller
			final Intent data = new Intent();
			data.putExtra(INTENT_PATH_PARAMETER, result.getPath());
			data.putExtra(INTENT_NAME_PARAMETER, result.getName());
			data.putExtra(INTENT_PARENT_PARAMETER, result.getParent());
			final SharedPreferences.Editor editor = mPrefs.edit();
			editor
				.putString(mLastPathPrefName, mDropboxPath)
				.commit();
			setResult(Activity.RESULT_OK, data);
			finish();
		}

		@Override
		protected void onProgressUpdate(Integer... values)
		{
			mDialog.setProgress(values[0]);
		}
	}

	@Override
	protected void initialize(Bundle savedInstanceState)
	{
		setTitle(R.string.menu_dbx_open_file);

		mDefaultPath = "/";
		mEnableDirScan = false;
		final String path = getIntent().getStringExtra(INTENT_PATH_PARAMETER);
		// XXX: call newEntry() only once!
		if (path != null)
			newEntry(path);
		else
			newEntry(mPrefs.getString(mLastPathPrefName, mDefaultPath));
	}

	@Override
	protected DialogEntry newEntry(String path)
	{
		new EntryDownloader(DbxOpenDialog.this).execute(path);
		return null;
	}

	/**
	 * Called when a list item is selected.
	 */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		final DialogEntry selectedItem = mPaths.get(position);
		if (selectedItem.isSelected())
		{
			new FileDownloader(DbxOpenDialog.this).execute(selectedItem.getPath());
			return;
		}
		super.onListItemClick(l, v, position, id);
	}

	/**
	 * Save configuration on pause.
	 */
	@Override
	protected void onPause()
	{
		super.onPause();
		if (mSelectedFile == null)
			return;
		// save selected file's path
		final SharedPreferences.Editor editor = mPrefs.edit();
		editor
			.putString(mLastPathPrefName, mCurrentEntry.getPath())
			.commit();
	}

	/**
	 * Save instance state.
	 */
	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putString(mLastPathPrefName, mCurrentEntry.getPath());
	}

	@Override
	protected String getLastPathPrefName()
	{
		return "dbx_browser_default_dir";
	}
}