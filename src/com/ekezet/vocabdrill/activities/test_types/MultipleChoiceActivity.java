package com.ekezet.vocabdrill.activities.test_types;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.adapters.WordListAdapter;
import com.ekezet.vocabdrill.kvtml.Kvtml;

public class MultipleChoiceActivity extends TestActivity
{
	private static WordListAdapter sAdapter = null;

	protected int mTestType = TEST_CHOICE;

	/**
	 * Event that handles answer selection.
	 */
	private final OnItemClickListener mOnItemClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> list, View item, int position, long id)
		{
			final TextView tv = (TextView) item.findViewById(R.id.rowtext);

			// check answer
			if (position == mQuestion.getSolutionIndex())
			{
				// right answer, removing entry from the list of questions
				if (!correct(mQuestion.getSolution()))
					// no more unanswered questions left
					return;
				else
					tv.setBackgroundResource(R.color.right_answer);
			} else
			{
				// wrong answer, increase number of mistakes
				tv.setBackgroundResource(R.color.wrong_answer);
				mistake(mQuestion.getSolution());
			}

			list.invalidate();

			try
			{
				// new thread for delaying the upcoming question
				new Thread(new Runnable()
				{
					@Override
					public void run()
					{
						try
						{
							Thread.sleep(Config.questionDelay);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						// trigger UI update after the delay
						runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								tv.setBackgroundResource(R.color.black);
								updateQuestion();
							}
						});
					}
				}).start();
			}
			catch (Exception e)
			{
				return;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// create and bind the string adapter for the list view
		ListView list = (ListView) findViewById(R.id.list);
		list.setOnItemClickListener(mOnItemClickListener);
		sAdapter = new WordListAdapter(this, R.id.rowtext);
		list.setAdapter(sAdapter);
		mMaxItems = Config.choiceNumber;
		restart();
	}

	@Override
	protected void updateQuestion(boolean configChange)
	{
		super.updateQuestion(configChange);
		if (mQuestion == null)
		{
			finish();
			return;
		}
		// populate list
		sAdapter.clear();
		Kvtml.Entry entry = null;
		for (int i = 0, I = mQuestion.getQuestionEntries().size(); i < I; i++)
		{
			entry = mQuestion.getQuestionEntries().get(i);
			sAdapter.add(String.valueOf(entry.translations.get(Config
				.getAnswerLangId()).text));
		}
	}

	@Override
	protected void loadContentView()
	{
		setContentView(R.layout.activity_multiple_choice);
	}
}