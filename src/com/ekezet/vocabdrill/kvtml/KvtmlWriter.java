package com.ekezet.vocabdrill.kvtml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Locale;

import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

/**
 * handles the saving of an Kvtml object
 */
public class KvtmlWriter
{
	private static final String FILE_ENCODING = "UTF-8";

	private Kvtml mKvtml = null;

	private String mFilename = null;

	/**
	 * compare two objects
	 */
	private class MyObjectComparator implements Comparator<Object>
	{
		@Override
		public int compare(Object lhs, Object rhs)
		{
			return Integer.valueOf((String) lhs).compareTo(
				Integer.valueOf((String) rhs));
		}
	}

	/**
	 * compare two strings
	 */
	private class MyStringComparator implements Comparator<String>
	{
		@Override
		public int compare(String lhs, String rhs)
		{
			return Integer.valueOf(lhs).compareTo(Integer.valueOf(rhs));
		}

	}

	public KvtmlWriter(Kvtml kvtml, String filename)
	{
		mKvtml = kvtml;
		mFilename = filename;
	}

	/**
	 * saves the Kvtml object to the SD card
	 *
	 * @return
	 */
	public boolean save()
	{
		File xmlFile = new File(Environment.getExternalStorageDirectory(),
			mFilename);
		try
		{
			xmlFile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException", "exception in createNewFile() method");
		}

		FileOutputStream outputStream = null;
		try
		{
			outputStream = new FileOutputStream(xmlFile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException", "can't create FileOutputStream");
		}

		XmlSerializer s = Xml.newSerializer();
		try
		{
			s.setOutput(outputStream, FILE_ENCODING);
			s.startDocument(null, Boolean.valueOf(true));
			s.docdecl(Kvtml.DOCTYPE);
			s.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output",
				true);

			// start document
			s.startTag(null, Kvtml.ROOT_TAG);
			s.attribute(null, "version", Kvtml.VERSION);

			/*
			 * Information section
			 */

			s.startTag(null, Kvtml.INFORMATION_TAG);

			s.startTag(null, Kvtml.GENERATOR_TAG);
			s.text("Vocab Drill");
			s.endTag(null, Kvtml.GENERATOR_TAG);

			s.startTag(null, Kvtml.TITLE_TAG);
			s.text(mFilename);
			s.endTag(null, Kvtml.TITLE_TAG);

			SimpleDateFormat sDF = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
			Calendar now = Calendar.getInstance();
			s.startTag(null, Kvtml.DATE_TAG);
			s.text(sDF.format(now.getTime()));
			s.endTag(null, Kvtml.DATE_TAG);

			s.endTag(null, Kvtml.INFORMATION_TAG);

			/*
			 * Identifiers section
			 */

			s.startTag(null, Kvtml.IDENTIFIERS_TAG);

			Object[] keys = mKvtml.identifiers.keySet().toArray();
			Arrays.sort(keys, new MyObjectComparator());
			Kvtml.Identifier id;
			for (int n = 0, mn = keys.length; n < mn; n++)
			{
				id = mKvtml.identifiers.get(keys[n]);
				s.startTag(null, Kvtml.IDENTIFIER_TAG);
				s.attribute(null, "id", id.id);

				s.startTag(null, Kvtml.NAME_TAG);
				s.text(id.name);
				s.endTag(null, Kvtml.NAME_TAG);

				s.startTag(null, Kvtml.LOCALE_TAG);
				s.text(id.locale);
				s.endTag(null, Kvtml.LOCALE_TAG);

				s.endTag(null, Kvtml.IDENTIFIER_TAG);
			}

			s.endTag(null, Kvtml.IDENTIFIERS_TAG);

			/*
			 * Entries section
			 */

			s.startTag(null, Kvtml.ENTRIES_TAG);

			keys = mKvtml.entries.keySet().toArray();
			// store ascending to improve readability in the file
			Arrays.sort(keys, new MyObjectComparator());
			Kvtml.Entry entry;
			Kvtml.Translation trans;
			for (int n = 0, mn = keys.length; n < mn; n++)
			{
				entry = mKvtml.entries.get(keys[n]);
				s.startTag(null, Kvtml.ENTRY_TAG);
				s.attribute(null, "id", entry.id);

				Object[] transKeys = entry.translations.keySet().toArray();
				Arrays.sort(transKeys, new MyObjectComparator());
				for (int k = 0; k < entry.translations.size(); k++)
				{
					trans = entry.translations.get(transKeys[k]);
					s.startTag(null, Kvtml.TRANSLATION_TAG);
					s.attribute(null, "id", trans.id);

					s.startTag(null, Kvtml.TEXT_TAG);
					s.text(trans.text);
					s.endTag(null, Kvtml.TEXT_TAG);

					// translation specific comment
					if (!trans.comment.equals(""))
					{
						s.startTag(null, Kvtml.COMMENT_TAG);
						s.text(trans.comment);
						s.endTag(null, Kvtml.COMMENT_TAG);
					}

					/*
					 * trans.id == "0" as long different Leitner-systems for different
					 * language combinations are not implemented
					 */
					if (!entry.date.equals("") && trans.id.equals("0"))
					{
						s.startTag(null, Kvtml.GRADE_TAG);
						s.startTag(null, Kvtml.CURRENT_GRADE_TAG);
						s.text(String.valueOf(entry.currentGrade));
						s.endTag(null, Kvtml.CURRENT_GRADE_TAG);
						s.startTag(null, Kvtml.COUNT_TAG);
						s.text(String.valueOf(entry.count));
						s.endTag(null, Kvtml.COUNT_TAG);
						s.startTag(null, Kvtml.ERROR_COUNT_TAG);
						s.text(String.valueOf(entry.errorCount));
						s.endTag(null, Kvtml.ERROR_COUNT_TAG);
						s.startTag(null, Kvtml.GRADE_DATE_TAG);
						s.text(entry.date);
						s.endTag(null, Kvtml.GRADE_DATE_TAG);
						s.endTag(null, Kvtml.GRADE_TAG);
					}

					s.endTag(null, Kvtml.TRANSLATION_TAG);

				}

				s.endTag(null, Kvtml.ENTRY_TAG);
			}

			s.endTag(null, Kvtml.ENTRIES_TAG);

			/*
			 * Lessons section
			 */
			s.startTag(null, Kvtml.LESSONS_TAG);
			keys = mKvtml.lessons.keySet().toArray();
			// sort to rewrite lessons in the same order they were parsed
			Arrays.sort(keys, new MyObjectComparator());
			Kvtml.Lesson lesson;
			Kvtml.Lesson sublesson;
			for (int n = 0, mn = keys.length; n < mn; n++)
			{
				String[] entryKeys;
				String[] sublessonKeys;

				lesson = mKvtml.lessons.get(keys[n]);

				s.startTag(null, Kvtml.CONTAINER_TAG);

				s.startTag(null, Kvtml.NAME_TAG);
				s.text(lesson.name);
				s.endTag(null, Kvtml.NAME_TAG);

				s.startTag(null, Kvtml.INPRACTICE_TAG);
				s.text(String.valueOf(lesson.inpractice));
				s.endTag(null, Kvtml.INPRACTICE_TAG);

				// write entries associated with the parent lesson
				entryKeys = lesson.keys.toArray(new String[lesson.keys.size()]);
				Arrays.sort(entryKeys, new MyStringComparator());
				for (int i = 0; i < lesson.keys.size(); i++)
				{
					s.startTag(null, Kvtml.ENTRY_TAG);
					s.attribute(null, "id", entryKeys[i]);
					s.endTag(null, Kvtml.ENTRY_TAG);
				}

				// write sublessons
				sublessonKeys = lesson.subLessons.keySet().toArray(
					new String[lesson.subLessons.size()]);
				Arrays.sort(sublessonKeys, new MyStringComparator());
				for (int i = 0; i < lesson.subLessons.size(); i++)
				{
					sublesson = lesson.subLessons.get(sublessonKeys[i]);

					s.startTag(null, Kvtml.CONTAINER_TAG);

					s.startTag(null, Kvtml.NAME_TAG);
					s.text(sublesson.name);
					s.endTag(null, Kvtml.NAME_TAG);

					s.startTag(null, Kvtml.INPRACTICE_TAG);
					s.text(String.valueOf(sublesson.inpractice));
					s.endTag(null, Kvtml.INPRACTICE_TAG);

					// write entries associated with the parent lesson
					entryKeys = sublesson.keys.toArray(new String[sublesson.keys.size()]);
					Arrays.sort(entryKeys, new MyStringComparator());
					for (int j = 0; j < sublesson.keys.size(); j++)
					{
						s.startTag(null, Kvtml.ENTRY_TAG);
						s.attribute(null, "id", entryKeys[j]);
						s.endTag(null, Kvtml.ENTRY_TAG);
					}

					s.endTag(null, Kvtml.CONTAINER_TAG);
				}

				s.endTag(null, Kvtml.CONTAINER_TAG);

			}

			s.endTag(null, Kvtml.LESSONS_TAG);

			// finishing document
			s.endTag(null, Kvtml.ROOT_TAG);
			s.endDocument();

			// write xml data into the FileOutputStream
			s.flush();
			outputStream.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			Log.e("Exception", "Couldn't save kvtml file.", e);
		}

		return true;
	}
}
