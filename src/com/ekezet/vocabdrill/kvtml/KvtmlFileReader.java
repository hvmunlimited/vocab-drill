package com.ekezet.vocabdrill.kvtml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * A FileReader class to track the number of bytes read from a file.
 */
public class KvtmlFileReader extends FileReader
{
	public interface OnReadingFileListener
	{
		/**
		 * Triggered on reading a file.
		 * 
		 * @param current Number of already read bytes.
		 * @param total The total number of bytes.
		 */
		public void onReadingFile(long current, long total);
	}
	
	private OnReadingFileListener mOnReadingFileListener = null;
	private long mFileSize;
	private long mCurrentByte;
	
	public KvtmlFileReader(String filename) throws FileNotFoundException
	{
		super(filename);
		mFileSize = (new File(filename)).length();
		mCurrentByte = 0;
		// create an empty listener so we can spare checks for null in the read()
		// method
		clearOnReadingFileListener();
	}
	
	@Override
	public int read(char[] buffer, int offset, int length) throws IOException
	{
		// increases the number of bytes read
		mCurrentByte += length;
		mOnReadingFileListener.onReadingFile(mCurrentByte, mFileSize);
		return super.read(buffer, offset, length);
	}
	
	public void setOnReadingFileListener(OnReadingFileListener listener)
	{
		if (listener == null)
			return;
		mOnReadingFileListener = listener;
	}
	
	public void clearOnReadingFileListener()
	{
		mOnReadingFileListener = new OnReadingFileListener()
		{
			@Override
			public void onReadingFile(long current, long total)
			{
			}
		};
	}
	
	public long getFileSize()
	{
		return mFileSize;
	}
}