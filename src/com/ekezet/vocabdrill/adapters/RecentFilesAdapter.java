package com.ekezet.vocabdrill.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.dialogs.RecentFilesDialog.RecentItem;

/**
 * A simple Adapter for dealing with an array of filenames.
 *
 * @author Kiripolszky Károly <karcsi@ekezet.com>
 * @see <a href="http://www.youtube.com/watch?v=wDBM6wVEO70">Google I/O 2010 -
 *      The world of ListView</a>
 */
public class RecentFilesAdapter extends ArrayAdapter<RecentItem>
{
	/**
	 * Static helper class for caching views.
	 *
	 * @author Kiripolszky Károly <karcsi@ekezet.com>
	 */
	private static class ViewHolder
	{
		public TextView text;
		public TextView text2;
	}

	private int mViewResource;

	/**
	 * Frequently used static handle for the layout inflater service.
	 */
	private static LayoutInflater sInflater;

	/**
	 * Overridden ArrayAdapter constructor.
	 */
	public RecentFilesAdapter(Context context, int textViewResourceId)
	{
		super(context, textViewResourceId);
		// get and store a handle for the layout inflater
		sInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mViewResource = textViewResourceId;
	}

	/**
	 * Returns the proper view to display a filename.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// prepare view cache
		ViewHolder holder;
		// view cache verification...
		if (convertView == null)
		{
			// ...inflate new layout resource
			convertView = sInflater.inflate(mViewResource, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.rowtext);
			holder.text.setTextColor(Color.WHITE);
			holder.text2 = (TextView) convertView.findViewById(R.id.rowtext2);
			// cache layout data as view tag
			convertView.setTag(holder);
		} else
			// ...use cached resource
			holder = (ViewHolder) convertView.getTag();

		// format list item (FIXME!)
		RecentItem entry = getItem(position);
		holder.text.setText(entry.getName());
		holder.text2.setText(entry.getDisplayPath());
		// return item view
		return convertView;
	}
}
