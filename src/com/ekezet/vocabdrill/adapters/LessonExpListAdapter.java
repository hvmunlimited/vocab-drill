package com.ekezet.vocabdrill.adapters;

import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.Kvtml.Lesson;

/**
 * implements the adapter to the HashMap containing the lessons
 */
public class LessonExpListAdapter extends BaseExpandableListAdapter
{
	/**
	 * context of the calling class
	 */
	private Context mContext;

	/**
	 * the lessons containing vocabulary entries
	 */
	private HashMap<String, Kvtml.Lesson> mLessons;

	/**
	 * ensures that a click on a parent lesson links to the checked state of the
	 * children
	 */
	private class MyOnParentCheckedChangeListener implements
			OnCheckedChangeListener
	{
		/**
		 * position in the list
		 */
		private int groupPosition;

		/**
		 * the object storing the lesson and its children
		 */
		private Kvtml.Lesson group;

		/**
		 * number of children
		 */
		private int childCount;

		public MyOnParentCheckedChangeListener(int groupPosition, Kvtml.Lesson group)
		{
			this.groupPosition = groupPosition;
			this.group = group;
			this.childCount = getChildrenCount(groupPosition);
		}

		/**
		 * if the checked state of the parent changes, set the state to all children
		 */
		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
			for (int i = 0; i < childCount; i++)
			{
				Kvtml.Lesson child = (Lesson) getChild(groupPosition, i);
				// also store the state in the underlying dataset
				child.inpractice = isChecked;
			}
			group.inpractice = isChecked;
			// update the state of the checkboxes
			notifyDataSetChanged();
		}
	}

	/**
	 * ensure that the state of a child is stored
	 */
	private class MyOnChildCheckedChangeListener implements
			OnCheckedChangeListener
	{
		/**
		 * the object of the child
		 */
		private Kvtml.Lesson child;

		public MyOnChildCheckedChangeListener(Kvtml.Lesson child)
		{
			this.child = child;
		}

		@Override
		public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
		{
			// store the state in the underlying dataset
			child.inpractice = isChecked;
		}

	}


	public LessonExpListAdapter(Context context,
		HashMap<String, Kvtml.Lesson> lessons)
	{
		mContext = context;
		mLessons = lessons;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition)
	{
		return mLessons.get(Integer.toString(groupPosition)).subLessons.get(Integer
			.toString(childPosition));
	}

	@Override
	public long getChildId(int groupPosition, int childPosition)
	{
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
		boolean isLastChild, View convertView, ViewGroup parent)
	{
		Kvtml.Lesson child = (Kvtml.Lesson) getChild(groupPosition, childPosition);
		// only create a new view if a previous view can't be reused
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_lesson_list, null);
		}
		TextView name = (TextView) convertView
			.findViewById(R.id.textView);
		CheckBox box = (CheckBox) convertView
			.findViewById(R.id.checkBox);
		// fill the view with data and set the corresponding listener
		name.setText(child.name);
		// reset the listener in case the view is reused
		box.setOnCheckedChangeListener(null);
		box.setChecked(child.inpractice);
		box.setOnCheckedChangeListener(new MyOnChildCheckedChangeListener(child));

		return convertView;
	}


	@Override
	public int getChildrenCount(int groupPosition)
	{
		return mLessons.get(Integer.toString(groupPosition)).subLessons.size();
	}

	@Override
	public Object getGroup(int groupPosition)
	{
		return mLessons.get(Integer.toString(groupPosition));
	}

	@Override
	public int getGroupCount()
	{
		return mLessons.size();
	}

	@Override
	public long getGroupId(int groupPosition)
	{
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
		View convertView, ViewGroup parent)
	{
		// only create a new view if a previous view can't be reused
		if (convertView == null)
		{
			LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.item_lesson_list, null);
		}
		Kvtml.Lesson group = (Kvtml.Lesson) getGroup(groupPosition);
		if (group == null)
			return convertView;
		TextView name = (TextView) convertView
			.findViewById(R.id.textView);
		CheckBox box = (CheckBox) convertView
			.findViewById(R.id.checkBox);
		// fill the view with data and set the corresponding listener
		name.setText(group.name);
		// reset the listener in case the view is reused
		box.setOnCheckedChangeListener(null);
		box.setChecked(group.inpractice);
		box.setOnCheckedChangeListener(new MyOnParentCheckedChangeListener(
			groupPosition, group));

		// draw the group indicator
		// ImageView iv = (ImageView)
		// convertView.findViewById(R.id.imageView_groupIndicator);
		// iv.setImageDrawable(null);
		// if(getChildrenCount(groupPosition)>0) {
		// iv.setImageResource(isExpanded? R.drawable.expander_ic_maximized :
		// R.drawable.expander_ic_minimized);
		// }
		return convertView;
	}


	@Override
	public boolean hasStableIds()
	{
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition)
	{
		return true;
	}
}
