package com.ekezet.vocabdrill.adapters;

import java.io.File;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;

/**
 * A simple Adapter for dealing with an array of filenames.
 *
 * @author Kiripolszky Károly <karcsi@ekezet.com>
 * @see <a href="http://www.youtube.com/watch?v=wDBM6wVEO70">Google I/O 2010 -
 *      The world of ListView</a>
 */
public class FileListAdapter extends ArrayAdapter<String>
{
	private String mParentDirText = null;
	/**
	 * Static helper class for caching views.
	 *
	 * @author Kiripolszky Károly <karcsi@ekezet.com>
	 */
	private static class ViewHolder
	{
		public TextView text;
	}

	private int mViewResource;

	/**
	 * Frequently used static handle for the layout inflater service.
	 */
	private static LayoutInflater sInflater;

	/**
	 * Overridden ArrayAdapter constructor.
	 */
	public FileListAdapter(Context context, int textViewResourceId)
	{
		super(context, textViewResourceId);
		// get and store a handle for the layout inflater
		sInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mViewResource = textViewResourceId;
	}

	/**
	 * Returns the proper view to display a filename.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// prepare view cache
		ViewHolder holder;
		// view cache verification...
		if (convertView == null)
		{
			// ...inflate new layout resource
			convertView = sInflater.inflate(mViewResource, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.rowtext);
			// cache layout data as view tag
			convertView.setTag(holder);
		} else
			// ...use cached resource
			holder = (ViewHolder) convertView.getTag();

		// format list item
		String s = getItem(position);
		holder.text.setText(s);
		if (s.endsWith(File.separator) || s.equals(mParentDirText))
		{
			holder.text.setText(Html.fromHtml(String.format("<b>%s</b>", s)));
			holder.text.setTextColor(Color.WHITE);
		} else
			// return item view
			holder.text.setTextColor(Color.LTGRAY);
		return convertView;
	}

	public String getParentDirText()
	{
		return mParentDirText;
	}

	public void setParentDirLabel(String parentDirText)
	{
		mParentDirText = parentDirText;
	}
}