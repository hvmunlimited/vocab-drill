package com.ekezet.vocabdrill.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.R;

/**
 * A simple Adapter for dealing with an array of strings.
 *
 * @author Kiripolszky Károly <karcsi@ekezet.com>
 * @see <a href="http://www.youtube.com/watch?v=wDBM6wVEO70">Google I/O 2010 -
 *      The world of ListView</a>
 */
public class WordListAdapter extends ArrayAdapter<String>
{
	/**
	 * Static helper class for caching views.
	 *
	 * @author Kiripolszky Károly <karcsi@ekezet.com>
	 */
	private static class ViewHolder
	{
		public TextView text;
	}

	/**
	 * Frequently used static handle for the layout inflater service.
	 */
	private static LayoutInflater sInflater;

	/**
	 * Overridden ArrayAdapter constructor.
	 */
	public WordListAdapter(Context context, int textViewResourceId)
	{
		super(context, textViewResourceId);
		// get and store a handle for the layout inflater
		sInflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	/**
	 * Returns the proper view to display a filename.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// prepare view cache
		ViewHolder holder;
		// view cache verification...
		if (convertView == null)
		{
			// ...inflate new layout resource
			convertView = sInflater.inflate(R.layout.item_word_list, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.rowtext);
			final int maxHeight = parent.getMeasuredHeight();
			final int height = maxHeight / Config.choiceNumber;
			holder.text.setHeight(height);
			// cache layout data as view tag
			convertView.setTag(holder);
		} else
			// ...use cached resource
			holder = (ViewHolder) convertView.getTag();

		// format list item
		holder.text.setText(getItem(position));
		// return item view
		return convertView;
	}
}