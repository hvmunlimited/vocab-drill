package com.ekezet.vocabdrill.adapters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.ekezet.vocabdrill.R;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.Kvtml.Entry;

/**
 * An Adapter for dealing with an array of KVTML entries.
 *
 * @author Kiripolszky Károly <karcsi@ekezet.com>
 * @see <a href="http://www.youtube.com/watch?v=wDBM6wVEO70">Google I/O 2010 -
 *      The world of ListView</a>
 */
public class EntryListAdapter extends ArrayAdapter<Kvtml.Entry> implements SectionIndexer, Filterable
{
	private List<Kvtml.Entry> mOriginalItems;
	private List<Kvtml.Entry> mItems;

	/**
	 * Static helper class for caching views.
	 *
	 * @author Kiripolszky Károly <karcsi@ekezet.com>
	 */
	private static class ViewHolder
	{
		public TextView text;
		public TextView text2;
	}

	private class EntryFilter extends Filter
	{
		@Override
		protected FilterResults performFiltering(CharSequence constraint)
		{
			Filter.FilterResults results = new Filter.FilterResults();
			if (constraint == null || constraint.length() == 0)
			{
				results.values = mOriginalItems;
				results.count = mOriginalItems.size();
			} else
			{
				List<Kvtml.Entry> filtered = new ArrayList<Kvtml.Entry>();
				String entryText;
				for (Kvtml.Entry entry : mOriginalItems)
				{
					entryText = entry.translations.get(mDisplayLanguageId).text.toLowerCase();
					if (entryText.startsWith(String.valueOf(constraint).toLowerCase()))
						filtered.add(entry);
				}
				results.values = filtered;
				results.count = filtered.size();
			}
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint, FilterResults results)
		{
			if (results.count == 0)
			{
				if (constraint != null && !constraint.toString().trim().equals(""))
				{
					mItems = new ArrayList<Kvtml.Entry>();
					updateAlphaIndices(mItems);
					notifyDataSetChanged();
				} else
					notifyDataSetInvalidated();
			} else
			{
				mItems = (ArrayList<Kvtml.Entry>) results.values;
				updateAlphaIndices(mItems);
				notifyDataSetChanged();
			}
		}
	}

	public static final String INDENT_STRING = "\t";

	private String mDisplayLanguageId = "";
	private HashMap<String, Integer> mAlphaIndices;
	private String[] mSections;
	private EntryFilter mFilter;

	/**
	 * Frequently used static handle for the layout inflater service.
	 */
	private static LayoutInflater sInflater = null;

	public EntryListAdapter(Context context, int textViewResourceId)
	{
		super(context, textViewResourceId);
		initialize(context);
	}

	public EntryListAdapter(Context context, int textViewResourceId, List<Kvtml.Entry> items, String displayLanguageId)
	{
		super(context, textViewResourceId, items);
		initialize(context);
		mDisplayLanguageId = displayLanguageId;
		mOriginalItems = items;
		mItems = items;
		updateAlphaIndices(items);
	}

	/**
	 * Returns the proper view to display a filename.
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		// prepare view cache
		ViewHolder holder;
		// view cache verification...
		if (convertView == null)
		{
			// ...inflate new layout resource
			convertView = sInflater.inflate(R.layout.item_entry_list, null);
			holder = new ViewHolder();
			holder.text = (TextView) convertView.findViewById(R.id.rowtext);
			holder.text2 = (TextView) convertView.findViewById(R.id.rowtext2);
			// cache layout data as view tag
			convertView.setTag(holder);
		} else
		// ...use cached resource
		holder = (ViewHolder) convertView.getTag();

		// format list item
		Kvtml.Entry entry = getItem(position);

		Object[] keys = entry.translations.keySet().toArray();
		String txt = "";
		String trans = "";
		String key = null;

		for (int i = 0, I = keys.length, x = 0; i < I; i++)
		{
			key = String.valueOf(keys[i]);
			trans = entry.translations.get(key).text.trim();
			if (!key.equals(mDisplayLanguageId) && 0 < trans.length())
			{
				txt += INDENT_STRING + trans;
				x++;
				if (x < (I - 1))
					txt += "\n";
			}
		}

		holder.text.setText(entry.translations.get(mDisplayLanguageId).text);
		holder.text2.setText(txt);

		// return item view
		return convertView;
	}

	@Override
	public int getCount()
	{
		return mItems.size();
	}

	@Override
	public Entry getItem(int position)
	{
		return mItems.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return mItems.get(position).hashCode();
	}

	@Override
	public int getPositionForSection(int section)
	{
		try
		{
			return mAlphaIndices.get(mSections[section]);
		} catch (ArrayIndexOutOfBoundsException e)
		{
			return 0;
		}
	}

	@Override
	public int getSectionForPosition(int position)
	{
		return 1;
	}

	@Override
	public Object[] getSections()
	{
		return mSections;
	}

	@Override
	public Filter getFilter()
	{
		if (mFilter == null)
			mFilter = new EntryFilter();
		return mFilter;
	}

	public String getDisplayLanguageId()
	{
		return mDisplayLanguageId;
	}

	public void setDisplayLanguageId(String displayLanguageId)
	{
		mDisplayLanguageId = displayLanguageId;
	}

	protected void updateAlphaIndices(List<Kvtml.Entry> items)
	{
		mAlphaIndices = new HashMap<String, Integer>();
		Kvtml.Entry entry = null;
		String letter = null;
		for (int i = 0, I = items.size(); i < I; i++)
		{
			entry = items.get(i);
			letter = entry.translations.get(mDisplayLanguageId).text.substring(0, 1).toUpperCase();
			if (!mAlphaIndices.containsKey(letter))
				mAlphaIndices.put(letter, i);
		}

		Set<String> letters = mAlphaIndices.keySet();
		ArrayList<String> sectionList = new ArrayList<String>(letters);
		Collections.sort(sectionList);
		mSections = new String[sectionList.size()];
		sectionList.toArray(mSections);
	}

	private void initialize(Context context)
	{
		// get and store a handle for the layout inflater
		if (sInflater == null)
			sInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
}