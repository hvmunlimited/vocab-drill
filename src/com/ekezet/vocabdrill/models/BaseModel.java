package com.ekezet.vocabdrill.models;

import android.content.ContentValues;
import android.database.Cursor;


public class BaseModel
{
	public static final String[] fields = new String[] { "_id" };
	
	private long mId = -1;
	
	public BaseModel()
	{
		
	}
	
	public BaseModel(long id)
	{
		setId(id);
	}
	
	public long getId()
	{
		return mId;
	}
	
	public void setId(long id)
	{
		mId = id;
	}
	
	public ContentValues getValues()
	{
		return getValues(false);
	}
	
	/**
	 * Returns the data associated with the model as a ContentValues object.
	 * 
	 * @param includeId
	 * @return
	 */
	public ContentValues getValues(boolean includeId)
	{
		ContentValues values = new ContentValues();
		if (!includeId)
			return values;
		values.put("_id", getId());
		return values;
	}
	
	public BaseModel populate(Cursor data)
	{
		return populate(data, true);
	}
	
	/**
	 * Populates the model with data.
	 * 
	 * @param data The data to be set.
	 * @param idIncluded True when the model already has an id.
	 * @return
	 */
	public BaseModel populate(Cursor data, boolean idIncluded)
	{
		if (data == null)
			return null;
		
		if (idIncluded)
			setId(data.getLong(0));
		
		return this;
	}
}
