package com.ekezet.vocabdrill.helpers;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import android.annotation.SuppressLint;
import android.util.Log;

import com.ekezet.vocabdrill.Config;
import com.ekezet.vocabdrill.kvtml.Kvtml;
import com.ekezet.vocabdrill.kvtml.interval.ParsedInterval;

/**
 * provide a simple way to filter the vocabulary data
 */
@SuppressLint("UseSparseArrays")
public class EntriesFilter implements Serializable
{
	/**
	 * used for serialization
	 */
	private static final long serialVersionUID = -2476363632100310243L;
	/*
	 * the currently implemented filters
	 */
	/**
	 * filters by the minimum number of errors a word has to have
	 */
	public static final int FILTER_FAILED = 0;
	/**
	 * only show newly entered words
	 */
	public static final int FILTER_NEW_WORDS = 1;
	/**
	 * filters all words that have been practiced before a given date
	 */
	public static final int FILTER_LAST_PRACTICED = 2;
	/**
	 * filters according to the grades of the Leitner-system
	 */
	public static final int FILTER_LEITNER = 3;

	/**
	 * filter a range of grades
	 */
	public static final int FILTER_GRADE_RANGE = 4;

	/**
	 * stores the filters
	 */
	private HashMap<Integer, String> mFilterList;

	public EntriesFilter()
	{
		mFilterList = new HashMap<Integer, String>();
	}

	/**
	 * add a filter to the filter list
	 */
	public void add(int filterType, String filterValue)
	{
		mFilterList.put(filterType, filterValue);
	}

	/**
	 * checks if the specified filter is to be applied
	 */
	public Boolean hasFilter(int filterType)
	{
		return mFilterList.containsKey(filterType);
	}

	/**
	 * filter all entries that fall into the selected lessons
	 *
	 * @param lessons
	 * @param source for the entries
	 * @return filtered entries
	 */
	private HashMap<String, Kvtml.Entry> filterLessons(
		HashMap<String, Kvtml.Lesson> lessons, HashMap<String, Kvtml.Entry> source)
	{
		HashMap<String, Kvtml.Entry> entries = new HashMap<String, Kvtml.Entry>();
		for (Kvtml.Lesson lesson : lessons.values())
		{
			if (lesson.inpractice)
			{
				for (String key : lesson.keys)
				{
					if (!entries.containsKey(key))
					{
						entries.put(key, source.get(key));
					}
				}
			}
			for (Kvtml.Lesson sublesson : lesson.subLessons.values())
			{
				if (sublesson.inpractice)
				{
					for (String key : sublesson.keys)
					{
						if (!entries.containsKey(key))
						{
							entries.put(key, source.get(key));
						}
					}
				}
			}
		}
		return entries;
	}

	/**
	 * filters by the Leitner system. to be called after filterLessons every grade
	 * corresponds to an time interval that has to elapse, before the entry is
	 * practiced again
	 */
	@SuppressLint("SimpleDateFormat")
	private void filterLeitner(HashMap<String, Kvtml.Entry> entries)
	{
		// ProgressDialog progressDialog = new ProgressDialog(context);
		// progressDialog.setProgress(0);
		// progressDialog.setMax(entries.size());
		// progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		// progressDialog.setMessage("Filtering Leitner System ...");
		// progressDialog.show();

		// initialize calendars
		Calendar[] compareCals = new Calendar[7];
		SimpleDateFormat df = new SimpleDateFormat(Kvtml.GRADE_DATE_DATE_FORMAT);
		// create calendars to compare date of entry against
		ParsedInterval interval = null;
		try
		{
			for (int i = 0, I = compareCals.length; i < I; i++)
			{
				compareCals[i] = Calendar.getInstance();
				interval = new ParsedInterval(Config.practiceIntervals[i]);
				addCalendar(compareCals[i], interval.toCalendar(), false);
			}
		}
		catch (ParseException e)
		{
		}
		Calendar entryCal = Calendar.getInstance();
		Iterator<Entry<String, Kvtml.Entry>> it = entries.entrySet().iterator();
		while (it.hasNext())
		{
			// progressDialog.setProgress(count);
			Kvtml.Entry entry = it.next().getValue();
			if (entry == null)
				break;
			// practice all entries of grade 0
			if (entry.currentGrade == 0)
				continue;
			try
			{
				// calendar with the time, where the entry has been practiced last
				entryCal.setTime(df.parse(entry.date));
			}
			catch (ParseException e)
			{
				Log.e("ParseException", "Error parsing entry date.", e);
			}
			// if the entry has been practiced before the practice interval elapsed, remove it
			if (entryCal.after(compareCals[entry.currentGrade - 1]))
				it.remove();
		}
		// progressDialog.dismiss();
		return;
	}

	// /**
	// * filters all words that have not been practiced before or with
	// currentGrade = 0
	// */
	// private void filterNewWords(HashMap<String, Kvtml.Entry> entries)
	// {
	// Iterator<Entry<String, com.ekezet.vocabdrill.kvtml.Kvtml.Entry>> it =
	// entries
	// .entrySet().iterator();
	// while (it.hasNext())
	// {
	// Map.Entry<String, Kvtml.Entry> nextEntry = it.next();
	// Kvtml.Entry entry = nextEntry.getValue();
	// // only practice new words
	// if (entry.currentGrade != 0)
	// it.remove();
	// }
	// }

	/**
	 * filters all words that fall into a specified grade range
	 *
	 * @param entries the data to be filtered
	 */
	private void filterGradeRange(HashMap<String, Kvtml.Entry> entries)
	{
		String[] rangeStr = mFilterList.get(EntriesFilter.FILTER_GRADE_RANGE)
			.split(":");
		int start = Integer.valueOf(rangeStr[0]);
		int end = Integer.valueOf(rangeStr[1]);

		Iterator<Entry<String, Kvtml.Entry>> it = entries.entrySet().iterator();
		while (it.hasNext())
		{
			Map.Entry<String, Kvtml.Entry> nextEntry = it.next();
			// entry can have null value here, probably coz of call to remove() below
			Kvtml.Entry entry = nextEntry.getValue();
			if (entry == null)
				continue;
			// filter out words that are not in the specified range
			if (entry.currentGrade < start || entry.currentGrade > end)
				it.remove();
		}
	}

	/**
	 * filters according to the specified filter rules
	 *
	 * @param entries the data to be filtered
	 */
	public HashMap<String, Kvtml.Entry> filter(HashMap<String, Kvtml.Entry> entries)
	{
		HashMap<String, Kvtml.Entry> filteredEntries = new HashMap<String, Kvtml.Entry>();
		if (!Config.lastData.lessons.isEmpty())
			filteredEntries.putAll(filterLessons(Config.lastData.lessons, entries));
		else
			filteredEntries.putAll(entries);

		if (hasFilter(EntriesFilter.FILTER_LEITNER))
			filterLeitner(filteredEntries);

		// if (hasFilter(Filter.FILTER_NEW_WORDS))
		// 	filterNewWords(filteredEntries);

		if (hasFilter(EntriesFilter.FILTER_GRADE_RANGE))
			filterGradeRange(filteredEntries);

		return filteredEntries;
	}

	/**
	 * Adds two calendars: target = target +- sum
	 *
	 * @param target target calendar
	 * @param sum summand calendar
	 * @param addition plus or minus
	 */
	private static void addCalendar(Calendar target, Calendar sum,
		boolean addition)
	{
		int sign = addition ? 1 : -1;
		target.add(Calendar.MONTH, sign * sum.get(Calendar.MONTH));
		// DAY_OF_MONTH falls in [1,30/31], therefore we have to store an
		// increment of 0 days as 1 day
		target.add(Calendar.DAY_OF_MONTH, sign
			* (sum.get(Calendar.DAY_OF_MONTH) - 1));
		target.add(Calendar.HOUR_OF_DAY, sign * sum.get(Calendar.HOUR_OF_DAY));
	}
}
