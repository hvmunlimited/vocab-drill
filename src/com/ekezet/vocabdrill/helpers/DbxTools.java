package com.ekezet.vocabdrill.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.content.Context;
import android.content.SharedPreferences;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.ProgressListener;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.exception.DropboxIOException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;
import com.ekezet.vocabdrill.Config;

public class DbxTools
{
	public final static String DBX_APP_KEY = "o1ccbbsx6yc7sjb";
	public final static String DBX_APP_SECRET = "2bk4dia7yqaf12c";

	public final static int DBX_LIST_LIMIT = 1000;

	public static final String PREF_DBX_TOKEN_KEY = "dbx_token_key";
	public static final String PREF_DBX_TOKEN_SECRET = "dbx_token_secret";

	final static public AccessType ACCESS_TYPE = AccessType.APP_FOLDER;

	private static DropboxAPI<AndroidAuthSession> sDbxApi = null;
	private static AndroidAuthSession sDbxSession = null;

	private static boolean sInitialized = false;

	public static void initialize(String appKey, String appSecret)
	{
		// try to get stored token pair
		final SharedPreferences prefs = Config.getPrefs();
		String key = prefs.getString(PREF_DBX_TOKEN_KEY, null);
		String secret = prefs.getString(PREF_DBX_TOKEN_SECRET, null);
		sDbxSession = new AndroidAuthSession(new AppKeyPair(
			appKey, appSecret), ACCESS_TYPE);
		// set access tokens if available
		if (key != null && secret != null)
			sDbxSession.setAccessTokenPair(new AccessTokenPair(key, secret));

		sDbxApi = new DropboxAPI<AndroidAuthSession>(sDbxSession);
		sInitialized = true;
	}

	public static void initialize()
	{
		String appKey = DBX_APP_KEY;
		String appSecret = DBX_APP_SECRET;
		initialize(appKey, appSecret);
	}

	public static DropboxAPI.Entry getEntry(String path, boolean list, int limit)
	{
		if (!sInitialized)
			return null;
		Entry entry = null;
		try
		{
			entry = sDbxApi.metadata(path, limit, null, list, null);
		}
		catch (DropboxIOException e)
		{

			return null;
		}
		catch (DropboxException e)
		{
			e.printStackTrace();
			return null;
		}
		return entry;
	}

	public static DropboxAPI.Entry getEntry(String path)
	{
		return getEntry(path, true, DBX_LIST_LIMIT);
	}

	public static void getFile(String path, File out, ProgressListener listener) throws FileNotFoundException, DropboxException
	{
		FileOutputStream fos = new FileOutputStream(out);
		sDbxApi.getFile(path, null, fos, listener);
	}

	public static boolean startAuthentication(Context context)
	{
		if (!sInitialized)
			return false;
		sDbxSession.startAuthentication(context);
		return true;
	}

	public static boolean finishAuthentication() throws IllegalStateException
	{
		if (!sInitialized)
			return false;
		if (!sDbxSession.authenticationSuccessful())
			return false;
		sDbxSession.finishAuthentication();
		return true;
	}

	public static AccessTokenPair getAccessTokenPair()
	{
		if (!sInitialized)
			return null;
		return sDbxSession.getAccessTokenPair();
	}

	public static DropboxAPI<AndroidAuthSession> getApi()
	{
		if (!sInitialized)
			return null;
		return sDbxApi;
	}

	public static boolean unlink()
	{
		if (!sInitialized)
			return false;
		sDbxSession.unlink();
		return true;
	}

	public static boolean isLinked()
	{
		if (!sInitialized)
			return false;
		return sDbxSession.isLinked();
	}
}